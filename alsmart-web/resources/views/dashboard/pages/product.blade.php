@extends('dashboard.layouts.main')

@section('content')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3">
    <h1 class="h2">Product</h1>
</div>
<div class="table-responsive">
  <table class="table table-sm">
    <thead>
      <tr>
        <th scope="col">No</th>
        <th scope="col">Gambar</th>
        <th scope="col">Name</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
      @forelse ($products as $product)
        <tr>
          <td>{{ $product->product_id }}</td>
          <td>{{ $product->gambar }}</td>
          <td>{{ $product->name }}</td>
          <td>
            <button type="button" class="btn btn-primary">Detail</button>
            <button type="button" class="btn btn-warning">Edit</button>
            <button type="button" class="btn btn-danger">Delete</button>

          </td>
        </tr>
      @empty
          
      @endforelse

    </tbody>
  </table>
</div>
@endsection
