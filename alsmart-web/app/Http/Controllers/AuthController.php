<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    //
    public function loginget()
    {
        return view('auth.login');
    }

    public function loginpost(Request $request)
    {
        $creadentials = $request->validate([
            'email' => 'required|email:dns',
            'password' => 'required'
        ]);

        if(Auth::attempt($creadentials)) {
            $request->session()->regenerate();

            return redirect()->intended('/dashboard');
        }
    }

    public function registerget()
    {
        return view('auth.register');
    }

    public function registerpost(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|max:255',
            'email' => 'required|email',
            'password' => 'required|min:5|max:255'
        ]);

        $validatedData['password'] = Hash::make($validatedData['password']);

        User::create($validatedData);

        $request->session()->flash('success', 'Registrasi user berhasil! Please login!');

        return redirect('/login');
    }
}
