<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('dashboard/index', function ()
{
    return view('pages.dashboard');
});

Route::get('/login', [AuthController::class, 'loginget']);
Route::post('/login', [AuthController::class, 'loginpost']);

Route::get('/register', [AuthController::class, 'registerget']);
Route::post('/register', [AuthController::class, 'registerpost']);

// Route::get('dashboard/product', function ()
// {
//     return view('product.index');
// });

Route::get('cuba', function () {
     return view('auth.login');
});

// Route::get('dashboard/product', [ProductController::class, 'index']);
// Route::get('dashboard/product/create', [ProductController::class, 'create']);

Route::resource('product', ProductController::class);