@extends('layouts.dashboard.main')

@section('content')
<div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
              <h5>Add Product</h5>
            </div>
            <form action="{{ route('product.store') }}" method="POST" enctype="multipart/form-data" class="form theme-form">

            @csrf

              <div class="card-body">
                <div class="row">
                  <div class="col">
                    <div class="mb-3 row">
                      <label class="col-sm-3 col-form-label">Name</label>
                      <div class="col-sm-9">
                        <input class="form-control @error('name') is-invalid @enderror" type="text" placeholder="Name Product" name="name" value="{{ old('name') }}">
                      </div>
                    </div>
                    <div class="row">
                        <div class="col">
                          <div class="mb-3 row">
                            <label class="col-sm-3 col-form-label">Upload Image</label>
                            <div class="col-sm-9">
                              <input class="form-control @error('image') is-invalid @enderror" type="file" name="image">
                            </div>
                          </div>
                        </div>
                    </div>
                    <div class="row">
                      <label class="col-sm-3 col-form-label">Deskripsi</label>
                      <div class="col-sm-9">
                        <textarea class="form-control @error('deskripsi') is-invalid @enderror" rows="5" cols="5" placeholder="Deskripsi" name="deskripsi">{{ old('deskripsi') }}</textarea>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-footer text-end">
                <div class="col-sm-9 offset-sm-3">
                  <button class="btn btn-primary" type="submit">Submit</button>
                  <input class="btn btn-light" type="reset" value="Cancel">
                </div>
              </div>
            </form>
          </div>
      </div>
    </div>
</div>
@endsection